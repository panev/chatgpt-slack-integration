import os
import openai
import subprocess
from slack_bolt.adapter.socket_mode import SocketModeHandler
from slack import WebClient
from slack_bolt import App
from dotenv import load_dotenv
from pymongo import MongoClient

# Debuging
import logging
logging.basicConfig(level=logging.INFO)
# END of Debuging

# Load environment variables
load_dotenv()

SLACK_BOT_TOKEN = os.getenv('BOT_TOKEN')
SLACK_APP_TOKEN = os.getenv('APP_TOKEN')
OPENAI_API_KEY  = os.getenv('GPT_TOKEN')
OPENAI_ENGINE   = os.getenv('GTP_ENGINE')
OPENAI_TOKENS   = int(os.getenv('GTP_TOKENS'))
MONGODB_URI     = os.getenv('MONGODB_URI')
MONGO_DB        = os.getenv('MONGODB')

# Initialize App and Client
app = App(token=SLACK_BOT_TOKEN)
client = WebClient(SLACK_BOT_TOKEN)

# Set the OpenAI API key
openai.api_key = OPENAI_API_KEY

# Connect to MongoDB
mongo_client = MongoClient(MONGODB_URI)
db = mongo_client[MONGO_DB]

# Function to log request and response to MongoDB
def log_request_response(user_id, user_name, request_text, response_text, timestamp):
    logs_collection = db['request_response_logs']
    logs_collection.insert_one({
        'user_id': user_id,
        'user_name': user_name,
        'request_text': request_text,
        'response_text': response_text,
        'timestamp': timestamp
    })

@app.event("app_mention")
def handle_message_events(body, logger):
    user_id = body["event"]["user"]
    user_info = client.users_info(user=user_id)
    user_name = user_info["user"]["real_name"]
    message_text = str(body["event"]["text"]).split(">")[1].strip()
    timestamp = body["event"]["ts"]

    # Debug: Log the content of the received message for troubleshooting
    logger.debug(f"Debug: Received message: '{message_text}'")

    logger.info(message_text)

    # Inform the user that the bot is processing the request
    client.chat_postMessage(
        channel=body["event"]["channel"],
        thread_ts=body["event"]["event_ts"],
        text="I'm on it! :robot_face: "
    )
    # Check if the command starts with "info"
    if message_text.startswith("info"):

        # Debug: Log the type of command being processed
        logger.debug(f"Debug: Processing 'info' command for message: '{message_text}'")
        ip_address = message_text.split(" ")[-1]
        result = subprocess.run(['python', 'retrieve_info.py', ip_address], capture_output=True, text=True)
        response_text = result.stdout

        client.chat_postMessage(
            channel=body["event"]["channel"],
            thread_ts=body["event"]["event_ts"],
            text=f"Information for IP {ip_address}: \n{response_text}"
        )
    # Check if the command starts with "curl"
    elif message_text.startswith("curl"):
        # Debug: Log the type of command being processed
        logger.debug(f"Debug: Processing 'curl' command for message: '{message_text}'")
        args = message_text.split()

        # If the user only enters "curl", provide an error message.
        if len(args) == 1:
            response_text = "Please provide a command. Use 'curl help' for available commands."
        else:
            command_list = ['python', 'run_curl.py']

            # Iterate through the args and clean up any potential Slack specific format, e.g., <http://domain|domain>
            for arg in args[1:]:
                clean_arg = arg.strip('<>').split('|')[-1]
                command_list.append(clean_arg)

            result = subprocess.run(command_list, capture_output=True, text=True)

            # Check if the command was successful
            if result.returncode == 0:
                response_text = result.stdout.strip()
            else:
                response_text = "An error occurred:\n" + result.stderr.strip()

        client.chat_postMessage(
            channel=body["event"]["channel"],
            thread_ts=body["event"]["event_ts"],
            text=response_text
        )

    # Check if the command starts with "zabbix"
    elif message_text.startswith("zabbix"):
        # Debug: Log the type of command being processed
        logger.debug(f"Debug: Processing 'zabbix' command for message: '{message_text}'")
        args = message_text.split()

        # If the user only enters "zabbix", provide an error message.
        if len(args) == 1:
            response_text = "Please provide a hostname or IP address after the 'zabbix' command."
        else:
            command_list = ['python', 'zabbix_script.py']

            # Iterate through the args and clean up any potential Slack specific format, e.g., <http://domain|domain>
            for arg in args[1:]:
                clean_arg = arg.strip('<>').split('|')[-1]
                command_list.append(clean_arg)

            result = subprocess.run(command_list, capture_output=True, text=True)

            # Check if the command was successful
            if result.returncode == 0:
                response_text = result.stdout.strip()
            else:
                response_text = "An error occurred:\n" + result.stderr.strip()

        client.chat_postMessage(
            channel=body["event"]["channel"],
            thread_ts=body["event"]["event_ts"],
            text=f"Zabbix Information for {clean_arg}: \n{response_text}"
        )

    else:
        gpt_response = openai.Completion.create(
            engine=OPENAI_ENGINE,
            prompt=message_text,
            max_tokens=OPENAI_TOKENS,
            n=1,
            stop=None,
            temperature=0.5
        ).choices[0].text.strip()

        response_text = gpt_response

        client.chat_postMessage(
            channel=body["event"]["channel"],
            thread_ts=body["event"]["event_ts"],
            text=f"Here you go: \n{response_text}"
        )

    log_request_response(user_id, user_name, message_text, response_text, timestamp)

if __name__ == "__main__":
    SocketModeHandler(app, SLACK_APP_TOKEN).start()