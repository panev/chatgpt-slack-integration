FROM python:3.9.17-slim-buster

RUN apt-get update && apt-get install -y iputils-ping curl \
    && apt-get clean \
    && rm --recursive --force /var/lib/apt/lists/*

HEALTHCHECK --interval=5s CMD ping -c 1 8.8.8.8

WORKDIR /app

COPY slack-bot.py requirements.txt retrieve_info.py  mongodb.py slack_db.py run_curl.py .env zabbix_script.py ./

RUN pip install --no-cache-dir -r requirements.txt

CMD ["python", "slack-bot.py"]
