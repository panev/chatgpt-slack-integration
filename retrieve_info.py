import os
import json
import sys
from mongodb import connect_to_mongo, get_db

def get_info_by_value(search_value, hosts_collection):
    # Try to find the record by public IP
    record = hosts_collection.find_one({"IP (public)": search_value})

    # If not found, try private IP
    if not record:
        record = hosts_collection.find_one({"IP (private)": search_value})

    # If not found, try Alias
    if not record:
        record = hosts_collection.find_one({"Alias": search_value})

    # If not found, try Hostname
    if not record:
        record = hosts_collection.find_one({"Hostname": search_value})

    if record:
        # Remove the MongoDB specific _id before converting to JSON
        record.pop('_id', None)
        return json.dumps(record, indent=4)
    else:
        return f"No information found for value: {search_value}"

if __name__ == "__main__":
    # Check if a command-line argument is provided
    if len(sys.argv) < 2:
        print("Please provide a search value as an argument.")
        sys.exit(1)

    value_to_search = sys.argv[1]

    # Connect to MongoDB
    client = connect_to_mongo()
    if client:
        db = get_db(client)
        hosts_collection = db.inventory
        
        # Use the command-line argument as the search value
        info = get_info_by_value(value_to_search, hosts_collection)
        print(info)
