import sys
import subprocess

def sanitize_domain(domain):
    domain = domain.strip('<>')
    if not domain.startswith(('http://', 'https://')):
        domain = 'https://' + domain
    return domain

def get_curl_arguments(command, domain):
    if command == "ttfb":
        return ["-o", "/dev/null", "-w", "Connect: %{time_connect} TTFB: %{time_starttransfer} Total time: %{time_total}", domain]
    # if command == "other_command":
    #     return ["other", "curl", "arguments"]
    return []

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Error: Invalid arguments. Usage: <command> <domain>")
        sys.exit(1)

    command = sys.argv[1]
    if command == "help":
        available_commands = {
            "ttfb": "Test Time to First Byte",
            "help": "Show available commands"
            # Add more commands here as needed
        }
        print("Available commands:")
        for cmd, desc in available_commands.items():
            print(f"{cmd}: {desc}")
        sys.exit(0)

    if len(sys.argv) < 3:
        print("Error: Invalid arguments. Usage: <command> <domain>")
        sys.exit(1)

    domain = sanitize_domain(sys.argv[2])

    curl_args = get_curl_arguments(command, domain)
    if not curl_args:
        print(f"Error: Unsupported command '{command}'. Use 'help' for a list of available commands.")
        sys.exit(1)

    # Execute the curl command with the provided arguments
    result = subprocess.run(["curl"] + curl_args, capture_output=True, text=True)
    
    if command == "ttfb":
        print(f"The TTFB value for {domain} is: {result.stdout.strip()}")
