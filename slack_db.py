import os
import sys
from slack_sdk import WebClient
from dotenv import load_dotenv
from io import StringIO

load_dotenv()

SLACK_BOT_TOKEN = os.getenv('BOT_TOKEN')
SLACK_APP_TOKEN = os.getenv('APP_TOKEN')

client = WebClient(token=SLACK_BOT_TOKEN)

class RedirectStdoutToSlack:
    def __init__(self, channel):
        self.channel = channel
        self._original_stdout = sys.stdout
        self._string_io = StringIO()

    def __enter__(self):
        sys.stdout = self._string_io
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        sys.stdout = self._original_stdout
        output = self._string_io.getvalue()
        send_message_to_slack(self.channel, output)


# def send_message_to_slack(channel, text):
#     response = client.chat_postMessage(channel=channel, text=text)
#     return response

def send_message_to_slack(channel, text):
    formatted_text = f"```\n{text}\n```"  # Wrap the text in a code block
    client.chat_postMessage(channel=channel, text=formatted_text)
