import requests
import os
import json
import sys
from dotenv import load_dotenv

# Load environment variables
load_dotenv()

ZAB_URL = os.getenv('ZABBIX_URL')
ZAB_USER = os.getenv('ZABBIX_USER')
ZAB_PASSWORD = os.getenv('ZABBIX_PASSWORD')

# Checking the number of arguments
if len(sys.argv) != 2:
    print("Usage: zabbix_script.py IP/HOSTNAME")
    sys.exit(1)

# Configuration
ZABBIX_URL = ZAB_URL
ZABBIX_USER = ZAB_USER
ZABBIX_PASSWORD = ZAB_PASSWORD
HOST_NAME = sys.argv[1]

headers = {
    "Content-Type": "application/json"
}

# Authentication
data = {
    "jsonrpc": "2.0",
    "method": "user.login",
    "params": {
        "user": ZABBIX_USER,
        "password": ZABBIX_PASSWORD
    },
    "id": 1
}
response = requests.post(ZABBIX_URL + "/api_jsonrpc.php", headers=headers, data=json.dumps(data))
token = response.json()["result"]

# Retrieving host information
data = {
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "filter": {
            "host": [HOST_NAME]
        }
    },
    "auth": token,
    "id": 2
}
response = requests.post(ZABBIX_URL + "/api_jsonrpc.php", headers=headers, data=json.dumps(data))

if not response.json()["result"]:
    print(f"Unable to find host with name: {HOST_NAME}")
    sys.exit(2)

host_id = response.json()["result"][0]["hostid"]

def get_item_value(token, host_id, key_):
    data = {
        "jsonrpc": "2.0",
        "method": "item.get",
        "params": {
            "hostids": host_id,
            "search": {
                "key_": key_
            }
        },
        "auth": token,
        "id": 3
    }
    response = requests.post(ZABBIX_URL + "/api_jsonrpc.php", headers=headers, data=json.dumps(data))
    items = response.json().get("result", [])
    
    if not items:
        print(f"Unable to retrieve value for key: {key_}")
        return "Unknown"
    
    return items[0]["lastvalue"]

load1 = get_item_value(token, host_id, "system.cpu.load[,avg1]")
load5 = get_item_value(token, host_id, "system.cpu.load[,avg5]")
load15 = get_item_value(token, host_id, "system.cpu.load[,avg15]")
cpu_wait = get_item_value(token, host_id, "system.cpu.util[,iowait]")
inodes = get_item_value(token, host_id, "vfs.fs.inode[/,pfree]")
memory = get_item_value(token, host_id, "vm.memory.size[pavailable]")
free_disk_space = get_item_value(token, host_id, "vfs.fs.size[/,pfree]")

#print(f"Load 1 for {HOST_NAME}: {load1}")
#print(f"Load 5 for {HOST_NAME}: {load5}")
#print(f"Load 15 for {HOST_NAME}: {load15}")
#print(f"CPU wait for {HOST_NAME}: {cpuiowait}")
#print(f"Available Memory for {HOST_NAME}: {memory} %")
#print(f"Free Disk Space for {HOST_NAME}: {free_disk_space} %")
#print(f"Free inodes for {HOST_NAME}: {inodes} %")
print(f"Load 1 for {HOST_NAME}: {format(float(load1), '.2f')}")
print(f"Load 5 for {HOST_NAME}: {format(float(load5), '.2f')}")
print(f"Load 15 for {HOST_NAME}: {format(float(load15), '.2f')}")
print(f"CPU wait for {HOST_NAME}: {format(float(cpu_wait), '.2f')}")
print(f"Available Memory for {HOST_NAME}: {round(float(memory))} %")
print(f"Free Disk Space for {HOST_NAME}: {round(float(free_disk_space))} %")
print(f"Free inodes for {HOST_NAME}: {round(float(inodes))} %")
