import os
from pymongo.mongo_client import MongoClient
from pymongo.server_api import ServerApi
from dotenv import load_dotenv

# Load environment variables
load_dotenv()

MONGODB_URI = os.getenv('MONGODB_URI')
MONGO_DB = os.getenv('MONGODB')

def connect_to_mongo():
    client = MongoClient(MONGODB_URI, server_api=ServerApi('1'))
    try:
        client.admin.command('ping')
        return client
    except Exception as e:
        print(e)
        return None

def get_db(client, db_name=MONGO_DB):
    return client[db_name]
